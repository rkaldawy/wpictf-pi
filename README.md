# wpictf-pi

Title: easy_as_pie

Category: reversing

Suggested points: 250

Description:

A reversing challenge where an inputted flag is parsed into offsets which are used
to find hex digits in pi. The challenge checks the digits to see if the flag is
correct. An Md5 checksum is used as an initial offset to prevent bruteforceing.

Instructions:

Build using make, and ONLY RELEASE THE BINARY. This is a reversing challenge where
they need to download and debug the local binary. Do not release anything else.