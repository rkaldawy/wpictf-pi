#include "pi.h"

int twoPowers[NUM_TWO_POWERS];

void InitializeTwoPowers()
{
  twoPowers[0] = 1.0;

  for (int i = 1; i < NUM_TWO_POWERS; i++)
  {
    twoPowers[i] = 2.0 * twoPowers[i-1];
  }
}

double ModPow16(double p, double m){
  int i;
  double pow1, pow2, result;

  if(m == 1.0){
    return 0.0;
  }

  for(i = 0; i < NUM_TWO_POWERS; i++){
    if(twoPowers[i] > p){
      break;
    }
  }
  
  pow2 = twoPowers[i-1];
  pow1 = p;
  result = 1.0;

  for(int j = 1; j <= i; j++){
    if (pow1 >= pow2){
      result = 16.0 * result;
      result = result - (int)(result/m) * m;
      pow1 = pow1 - pow2;
    }

    pow2 = 0.5 * pow2;
    if(pow2 >= 1.0){
      result = result * result;
      result = result - (int)(result/m) * m;
    }
  }

  return result;
}

//find the nth digit of pi
double series(int m, int n){
  double denom; double power; double sum = 0.0; double term;

  for(int k = 0; k < n; k++)
  {
    denom = 8*k + m;
    power = n-k;
    //term = fmod(pow(16.0, power), denom);
    term = ModPow16(power, denom);
    sum += term/denom;
    sum -= (int)sum;
  }

  for(int k = n; k <= n + 100; k++){
    denom = 8*k + m;
    power = n-k;
    term = pow(16.0, power) / denom;

    if(term < EPSILON){
      break;
    }

    sum += term;
    sum -= (int)sum;
  }

  return sum;
}

int findDigit(int n)
{
  double s1 = series(1, n); 
  double s2 = series(4, n);
  double s3 = series(5, n);
  double s4 = series(6, n);

  double res = 4.0*s1 - 2.0*s2 - s3 - s4;
  res = res - (int)res + 1.0;
  res = (res - (int)res) * 16.0;
  return res;
}

void findDigits(int n, int* ret, int count)
{
  double s1 = series(1, n); 
  double s2 = series(4, n);
  double s3 = series(5, n);
  double s4 = series(6, n);

  double res = 4.0*s1 - 2.0*s2 - s3 - s4;
  res = res - (int)res + 1.0;
  for(int i = 0; i < count; i++){
    res = (res - (int)res) * 16.0;
    ret[i] = res;
  }
}

/*void main()
{
  InitializeTwoPowers();
  char test[] = "WPI{Sw33TeR_tH@n_aH_cH3rRi3_P1e}";
  int offset; char* input = test; int res[3]; int total = 0;
  for(int i = 0; i < 16; i++){
    offset = 0;
    memcpy((char*)&offset, input, 2);
    total += offset;
    findDigits(total, res, 3);
    //printf("%c%c: %x %x %x\n", *input, *(input+1), res[0], res[1], res[2]);
    printf("{0x%x, 0x%x, 0x%x},\n", res[0], res[1], res[2]);
    input += 2;
  } 
}*/
