#ifndef PI_H
#define PI_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <openssl/md5.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <openssl/opensslv.h>
#include <openssl/crypto.h>

#define EPSILON 1e-17
#define NUM_TWO_POWERS 32
#define OFFSET_COUNT 16

extern int twoPowers[NUM_TWO_POWERS];

//calculates one of the four sums required to find a digit
double series(int m, int n);
//find a hex digit in pi, given an offset
int findDigit(int n);
//find a set of sequentiial hex digits of pi
void findDigits(int n, int* ret, int count);
//dp initialization
void InitializeTwoPowers();

#endif
