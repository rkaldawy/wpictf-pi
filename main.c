#include "pi.h"

//sha256 hash of flag
char hash[32] = {0xc2, 0x71, 0x25, 0x6e, 0xf0, 0xa7, 0xbf, 0xea, 0x41, 0xf4, 0x83, 0xf1, 0x6b, 0x83, 0xaa, 0xf0, 0xd9, 0xb6, 0x6b, 0xbf, 0x64, 0x83, 0x3, 0x83, 0xac, 0xd3, 0x6f, 0xc3, 0x1d, 0x55, 0xe6, 0xe2};

//with checksum
int digits[OFFSET_COUNT][3] = {
  {0xe, 0x8, 0x9},
  {0xb, 0x8, 0x7},
  {0x8, 0xa, 0xd},
  {0x7, 0xc, 0x7},
  {0xe, 0x7, 0xc},
  {0x3, 0xa, 0x3},
  {0x4, 0x2, 0x5},
  {0x2, 0x7, 0x5},
  {0xb, 0x5, 0x4},
  {0x1, 0xa, 0x6},
  {0x2, 0xe, 0xd},
  {0x1, 0xc, 0x5},
  {0xf, 0xd, 0xf},
  {0x3, 0x3, 0x1},
  {0x2, 0xd, 0xb},
  {0x7, 0x4, 0xf}};

int main(int argc, char **argv)
{
  InitializeTwoPowers();
  printf("Running...\n");
  if(argc != 2){
    return 0;
  } 

  char* input = argv[1];
  char* head = input;
  if(strlen(input) != 2 * OFFSET_COUNT){
    return 0;
  }

  int total = 0; int offset; int val[3]; int result;

  uint8_t digest[16];
  int checksum = 0; 
  MD5_CTX context;

  MD5_Init(&context);
  MD5_Update(&context, input, strlen(input));
  MD5_Final(digest, &context);
  memcpy(&checksum, digest, 2);
  total += checksum;

  for(int i = 0; i < OFFSET_COUNT; i++){
    offset = 0;
    memcpy((char*)&offset, input, 2);
    total += offset;
 
    findDigits(total, val, 3);

    //printf("%c%c:\n", *input, *(input+1));
    //printf("%x %x %x\n %x %x %x\n", val[0], val[1], val[2],
    //                                digits[i][0], digits[i][1], digits[i][2]);
    if(val[0] != digits[i][0] ||
       val[1] != digits[i][1] ||
       val[2] != digits[i][2]){
      return 0;
    }

    input += 2;
  }

  uint8_t sha_digest[32];
  SHA256_CTX sha_ctx;
  SHA256_Init(&sha_ctx);
  SHA256_Update(&sha_ctx, head, strlen(head));
  SHA256_Final(sha_digest, &sha_ctx);
  if(!(strncmp(sha_digest, hash, 32))){
    printf("Correct!\n");
  }

  return 1;
} 
