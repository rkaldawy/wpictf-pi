all: pi

pi: pi.o main.o
	gcc -o pi pi.o main.o -lm -lcrypto -lssl -lz

main.o: main.c 
	gcc -c main.c pi.h

pi.o: pi.c 
	gcc -c pi.c pi.h

clean:
	rm -rf pi *.o *.gch
